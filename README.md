# key-react-test
## Getting started

First clone this repository.

Go to the proyect folder

Install project dependencies
```shell
npm install 
```
When installing dependencies was finished launch project with the following command
```shell
npm run start
```
You be able to see the project with the following URL [`http://localhost:3000/`](http://localhost:3000/)

## Project commands
Main commands served from `package.json`

- `npm run start` ->  Compiles and hot-reloads for development
- `npm run test` -> Run your unit tests
- `npm run open` -> Run your end-to-end tests in your browser
- `npm run cypress` -> Run your end-to-end tests

