/// <reference types="Cypress" />
it('should do full successfull path', () => {
    cy.visit('http://localhost:3000/');
    cy.get('.language > :nth-child(2)');
    cy.get('.language > :nth-child(1)')
    cy.get(':nth-child(1) > div > input').click();
    cy.get(':nth-child(2) > div > input').click();
    cy.get('.WelcomeScreenFooter > button').click();
    cy.get('.footer :nth-child(2)').click();
    cy.get(':nth-child(1) > .inputContainer > input').type('pruebaOK123');
    cy.get(':nth-child(2) > .inputContainer > input').type('pruebaOK123');
    cy.get('textarea').type('Succsess');
    cy.get('.footerForm :nth-child(2)').click();
    cy.get('.button > button').click();
})

it('should do full path not successfull', () => {
    cy.visit('http://localhost:3000/');
    cy.get('.language > :nth-child(2)');
    cy.get('.language > :nth-child(1)')
    cy.get(':nth-child(1) > div > input').click();
    cy.get(':nth-child(2) > div > input').click();
    cy.get('.WelcomeScreenFooter > button').click();
    cy.get('.footer :nth-child(2)').click();
    cy.get(':nth-child(1) > .inputContainer > input').type('pruebaKO123');
    cy.get(':nth-child(2) > .inputContainer > input').type('pruebaKO123');
    cy.get('textarea').type('Error');
    cy.get('.footerForm :nth-child(2)').click();
    cy.get('.button > button').click();
})

it('should cancel create password process from first screen', () => {
    cy.visit('http://localhost:3000/');
    cy.get('.language > :nth-child(2)');
    cy.get('.language > :nth-child(1)')
    cy.get(':nth-child(1) > div > input').click();
    cy.get(':nth-child(2) > div > input').click();
    cy.get('.WelcomeScreenFooter > button').click();
    cy.get('.footer :nth-child(1)').click();
})

it('should cancel create password process second screen', () => {
    cy.visit('http://localhost:3000/');
    cy.get('.language > :nth-child(2)');
    cy.get('.language > :nth-child(1)')
    cy.get(':nth-child(1) > div > input').click();
    cy.get(':nth-child(2) > div > input').click();
    cy.get('.WelcomeScreenFooter > button').click();
    cy.get('.footer :nth-child(2)').click();
    cy.get(':nth-child(1) > .inputContainer > input').type('pruebaOK123');
    cy.get(':nth-child(2) > .inputContainer > input').type('pruebaOK123');
    cy.get('textarea').type('Succsess');
    cy.get('.footerForm :nth-child(1)').click();
})