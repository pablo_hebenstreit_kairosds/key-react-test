import React from 'react'
import './welcomeScreen.css' 
import { useTranslation } from "react-i18next";
import { Button } from '../../components/Button'
import { CheckBox } from '../../components/CheckBox'
import { HeaderLayout } from '../../components/HeaderLayout'
import { Modal } from '../../components/Modal'
import logoKey from '../../assets/img/key_openbank.png'


export const WelcomeScreen = ({changeLanguage}) => {

    const { t } = useTranslation('translation');

    const [isCheckedAge, setIsCheckedAge] = React.useState(false);
    const [isCheckedPolicy, setIsCheckedPolicy] = React.useState(false);
    const [modalVisible, setModalVisible] = React.useState(false)

    const handleAgeChange = async () => {

        setIsCheckedAge(!isCheckedAge)

    };

    const handlePolicyChange = () => {

        setIsCheckedPolicy(!isCheckedPolicy);

    };

  return (      
      
    <>  

        <div className='header'>

            <span className='logo'>

                <img src={logoKey} alt="logo" />

            </span>

            <div className='language'>

                <Button 
                    backgroundColor='transparent'
                    color='#002B45'
                    text='ES'
                    border='none'
                    onClick={() =>changeLanguage('es')}
                />

                /

                <Button 
                    backgroundColor='transparent'
                    color='#002B45'
                    text='EN'
                    border='none'
                    onClick={() =>changeLanguage('en')}
                />
                
            </div>

        </div>

        <HeaderLayout title={t('welcomeScreen.title')} />
        
        <div>

            <p>{t('welcomeScreen.tenMinutes')}</p>
            <h4>{t('welcomeScreen.step1Title')}</h4> 
            <p>{t('welcomeScreen.step1Text')}</p>

            <h4>{t('welcomeScreen.step2Title')}</h4>
            <p>{t('welcomeScreen.step2Text')}</p>

            <h4>{t('welcomeScreen.step3Title')}</h4>
            <p>{t('welcomeScreen.step3Text')}</p>

        </div>

        <hr style={{borderTop: '1px solid lightgray', margin: '45px 0px'}} />
        
        <div className='WelcomeScreenFooter'>

            <div>

                <CheckBox label={t('welcomeScreen.age')} isChecked={isCheckedAge} handleOnChange={handleAgeChange}/>
                <CheckBox label={t('welcomeScreen.policy')} isChecked={isCheckedPolicy} handleOnChange={handlePolicyChange}/>

            </div>

            <Button 
                backgroundColor='#002B45'
                color='white'
                disabled={!isCheckedAge || !isCheckedPolicy}
                height='40px'
                marginTop='10px'
                onClick={() => setModalVisible(true)}
                text={t('welcomeScreen.button')}
                width='120px'
            />

        </div>
              
        { 
            modalVisible && <Modal onClickClose={() => setModalVisible(false)} />
        }
        
    </>    

  )

}