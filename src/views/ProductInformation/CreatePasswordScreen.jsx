import React from "react";
import './createPasswordScreen.css'
import { useTranslation } from "react-i18next";
import { Card } from "../../components/Card";
import { MainLayout } from "../../layouts/MainLayout";
import head from '../../assets/img/group.svg'
import safetyBox from '../../assets/img/group-3.svg'


export const CreatePasswordScreen = ({onClickClose, onNext}) => {

    const { t } = useTranslation('translation');

  return (

    <>  

        <div className='header'>

            <div className='headerDots'>

                <span className="triangle">

                    <div className='blueDot'>1</div>
                    <div className='triangle-bottom'></div>

                </span>

                <hr align="left"
                    color= '#002B45'
                    size="2"
                    width="50px;"
                />

                <div className='blueDot'>2</div>

                <hr align="left"
                    color= '#002B45'
                    size="2"
                    width="50px;"
                />

                <div className='blueDot'>3</div>

            </div>

        </div>

        <MainLayout title={t('createPasswordScreen.title')} onClickClose={onClickClose} onNext={onNext}>

            <div className="cardContainer">

                <Card img={head} align='column' text={t('createPasswordScreen.textCardLeftImage')} />
                
                <Card img={safetyBox} align='column' text={t('createPasswordScreen.textCardRightImage')} />

            </div>

            <div className="textContainer">

                <div className="firstTextBlock">

                    <h4>{t('createPasswordScreen.textCardLeftTitle')}</h4>

                    <p>{t('createPasswordScreen.textCardLeft')}</p>

                </div>
                
                <div>

                    <h4>{t('createPasswordScreen.textCardRightTitle')}</h4>

                    <p>{t('createPasswordScreen.textCardRight')}</p>

                </div>

            </div>

        </MainLayout>              
       

    </> 

  )

}
