import React from 'react'
import './feedback.css' 
import { useTranslation } from "react-i18next";
import { Button } from '../../components/Button'
import iconError from '../../assets/img/iconError.png'

export const FeedbackErrorScreen = ({onClickClose}) => {

  const { t } = useTranslation('translation');

  React.useEffect(() => {

      window.scrollTo(0, 0);
      
  }, [])

  return (

    <>

        <div className='container' >
    
            <img src={iconError} alt='img' className='image'/>
    
            <div >
    
                <span>
    
                    <h2>{t('feedbackErrorScreen.title')}</h2>          
                    <p><strong>{t('feedbackErrorScreen.text')}</strong></p>
                    
                </span>
    
    
            </div>
    
        </div>
        
        <hr style={{borderTop: '1px solid lightgray', margin: '45px 0px'}} />
    
        <span className='button'>
            <Button 
              backgroundColor='white'
              color='red'
              border='none'
              onClick={onClickClose}
              text={t('feedbackErrorScreen.buttonText')}
            />
        </span>

    </>

  )

}
