import React from 'react'
import './feedback.css' 
import { useTranslation } from "react-i18next";
import { Button } from '../../components/Button'
import iconSuccess from '../../assets/img/iconSuccess.png'

export const FeedbackSuccessScreen = ({onClickClose}) => {

    const { t } = useTranslation('translation');

    React.useEffect(() => {

        window.scrollTo(0, 0);
        
    }, [])

    return (

        <>
    
            <div className='container' >
        
                <img src={iconSuccess} alt='img' className='image'/>
        
                <div >
        
                    <span>
        
                        <h2>{t('feedbackSuccessScreen.title')}</h2>          
                        <p><strong>{t('feedbackSuccessScreen.text')}</strong></p>
                        
                    </span>
        
        
                </div>
        
            </div>
            
            <hr style={{borderTop: '1px solid lightgray', margin: '45px 0px'}} />
        
            <span className='button'>
                <Button 
                  backgroundColor='white'
                  border='none'
                  color='red'
                  onClick={onClickClose}
                  text={t('feedbackSuccessScreen.buttonText')}
                />
            </span>
    
        </>
    
      )
    
    }
