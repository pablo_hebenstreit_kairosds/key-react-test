import React from 'react'
import './formScreen.css'
import { useTranslation } from "react-i18next";
import { Button } from '../../components/Button'
import { HeaderLayout } from "../../components/HeaderLayout.jsx"
import { Input } from "../../components/Input.jsx"
import { submitForm } from '../../services/api.js'
import whiteCheck from '../../assets/img/icons8-check-48.png'
import eye from '../../assets/img/eye-visible.svg'
import eyeCrossed from '../../assets/img/eye-non-visible.svg'


export const FormScreen = ({onClickClose, onNext}) => {

    const { t } = useTranslation('translation');
    
    const [cluePassword, setCluePassword] = React.useState('')
    const [createPassword, setCreatePassword] = React.useState('')
    const [repeatPassword, setRepeatPassword] = React.useState('')
    const [samePassword, setSamePassword] = React.useState(false)
    const [validPassword, setValidPassword] = React.useState(false)

    React.useEffect(() => {

        window.scrollTo(0, 0);
        
    }, [])

    const isAValidPassword = (value) => {     

        const regex = new RegExp(/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,24}$/);
    
        if(regex.test(value)){

            setValidPassword(true) 
            setCreatePassword(value) 

        } else {

            setValidPassword(false) 

        }
    }

    const checkPasswordCreate = (value1) => {

        if(repeatPassword === value1) {

            setSamePassword(true)

        } else {

            setSamePassword(false)

        }
        
    }

    const checkPasswordRepeat = (value2) => {

        if(createPassword === value2) {

            setSamePassword(true)

        } else {

            setSamePassword(false)

        }
        
    }
    
    const sendValues = async () => {

        try {
            
            const response = await submitForm(createPassword) 
            
            if(response.status === 200){

                onNext('FeedbackSuccessScreen')  

            } 

        } catch (error) {

            onNext('FeedbackErrorScreen')

        }

    }

   
  return (

    <> 

        <div className='header'>

            <div className='headerDots'>

                <div className='redDot'>

                    <img src={whiteCheck} alt="whiteCheck" className='redDotImage'/>

                </div>

                <hr align="left"
                    color= '#d41e3f'
                    size="2"
                    width="50px;"
                />

                <span className="triangle">

                    <div className='blueDot'>2</div>
                    <div className='triangle-bottom'></div>

                </span>

                <hr align="left"
                    color= '#4c5c67'
                    size="2"
                    width="50px;"
                />

                <div className='blueDot3'>3</div>

            </div>

        </div>  

        <HeaderLayout title={t('formScreen.title')}/>

        <div className="formContainer">

            <p>{t('formScreen.firstTextA')}<br/>{t('formScreen.firstTextB')}</p>

            <div className='formInputs'>

                <span className='singleInput'>

                    <Input 
                        height='34px' 
                        label={t('formScreen.firstInputLabel')} 
                        onChange={e => {(setCreatePassword(e.target.value)); isAValidPassword(e.target.value); checkPasswordCreate(e.target.value)}}
                        placeholder={t('formScreen.firstInputPlaceholder')}
                    />

                    { 
                        createPassword.length === 0
                        ? <small className='length'>{t('formScreen.validatorPasswordErrorA')}<br/> {t('formScreen.validatorPasswordErrorB')}</small>
                        : (
                            validPassword 
                            ? <small className='correct'>{t('formScreen.validatorPasswordSuccess')}</small> 
                            : <small className='error'>{t('formScreen.validatorPasswordErrorA')}<br/> {t('formScreen.validatorPasswordErrorB')}</small>
                        )
                    }

                </span>

                <span className='singleInput'>
                    

                    <Input 
                        height='34px' 
                        label={t('formScreen.secondInputLabel')} 
                        onChange={e => {(setRepeatPassword(e.target.value)); checkPasswordRepeat(e.target.value)}}
                        placeholder={t('formScreen.secondInputPlaceholder')}  
                    /> 
                        
                    { 
                        repeatPassword.length === 0
                        ? null
                        : (
                            samePassword 
                            ? <small className='correct'>{t('formScreen.validatorSamePasswordSuccess')}</small> 
                            : <small className='error'>{t('formScreen.validatorSamePasswordError')}</small>
                        )
                    }

                </span>

            </div>

        </div>

        <div className="">

            <div className="firstTextBlock">

                <p>{t('formScreen.hintTextA')}</p>

                <p><strong>{t('formScreen.hintTextB')}</strong></p>

                <textarea 

                    cols='164'
                    maxLength={255} 
                    name="textarea" 
                    onChange={e => (setCluePassword(e.target.value))}
                    placeholder={t('formScreen.textareaPlaceholder')}
                    rows="1" 
                    />                    

                <p>{cluePassword.length}/255</p>
            
            </div>

        </div>

        <hr style={{borderTop: '1px solid lightgray', margin: '45px 0px'}} />

        <div className='footerForm'>

            <Button 
                backgroundColor='white'
                color='#002B45'
                text={t('formScreen.leftButton')}
                border='none'
                onClick={onClickClose}
            />

            <Button 
                backgroundColor='#002B45'
                color='white'
                text={t('formScreen.rightButton')}
                height='40px'
                width='120px'
                onClick={sendValues} 
                disabled={!validPassword || !samePassword }
            />   

        </div>                

    </> 

  )

}