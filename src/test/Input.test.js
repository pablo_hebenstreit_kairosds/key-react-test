import React from 'react'
import { render, screen } from '@testing-library/react';

import { Input } from '../components/Input'

describe('Input', () => {

    test('Input renders', () => {

        expect(

            render(

                <Input label='Input text label' />

            )

        ).toMatchSnapshot();

        screen.getByText('Input text label');

    });

}); 