import React from 'react'
import { render, screen } from '@testing-library/react';
import { WelcomeScreen } from '../views/WelcomeScreen/WelcomeScreen'

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  useTranslation: () => {
    return {
      t: (str) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('WelcomeScreen', () => {

  test('WelcomeScreen render', () => {
    
    expect(

      render(

        <WelcomeScreen />

      )

    ).toMatchSnapshot();

    screen.getByText('welcomeScreen.title');    
    
  });

}); 