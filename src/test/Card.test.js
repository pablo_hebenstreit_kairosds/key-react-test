import React from 'react'
import { render, screen } from '@testing-library/react';

import { Card } from '../components/Card'

describe('Card', () => {

    test('Card renders', () => {

        expect(

            render(

                <Card text='Text inside a card' />

            )

        ).toMatchSnapshot();

        screen.getByText('Text inside a card');

    });

}); 