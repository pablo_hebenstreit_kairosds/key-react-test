import React from 'react'
import { render, screen } from '@testing-library/react';

import { HeaderLayout } from '../components/HeaderLayout'

describe('HeaderLayout', () => {

  test('HeaderLayout renders', () => {

    expect(

      render(

        <HeaderLayout title='HeaderLayout title' />

      )

    ).toMatchSnapshot();

    screen.getByText('HeaderLayout title');
    
  });

}); 