import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react';
import { FormScreen } from '../views/Form/FormScreen'

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  useTranslation: () => {
    return {
      t: (str) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('FormScreen', () => {

  test('FormScreen render', () => {

    const onClickClose = jest.fn();

    expect(

      render(

        <FormScreen onClickClose={onClickClose} />

      )

    ).toMatchSnapshot();

    fireEvent(
        screen.getByText('formScreen.leftButton'),
        new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
        })
    );
    
    expect(onClickClose).toBeCalledTimes(1);

    screen.getByText('formScreen.title');    
    
  });

}); 