import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react';
import { FeedbackSuccessScreen } from '../views/Feedback/FeedbackSuccessScreen'

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  useTranslation: () => {
    return {
      t: (str) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('FeedbackSuccessScreen', () => {

  test('FeedbackSuccessScreen render', () => {

    const onClickClose = jest.fn();

    expect(

      render(

        <FeedbackSuccessScreen onClickClose={onClickClose} />

      )

    ).toMatchSnapshot();

    fireEvent(
        screen.getByText('feedbackSuccessScreen.buttonText'),
        new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
        })
    );
    
    expect(onClickClose).toBeCalledTimes(1);

    screen.getByText('feedbackSuccessScreen.title');    
    
  });

}); 