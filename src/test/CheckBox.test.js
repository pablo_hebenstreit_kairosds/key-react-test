import React from 'react'
import { render, screen } from '@testing-library/react';

import { CheckBox } from '../components/CheckBox'

describe('CheckBox', () => {

    test('CheckBox renders', () => {

        expect(

            render(

                <CheckBox label='Checkbox text label' />

            )

        ).toMatchSnapshot();

        screen.getByText('Checkbox text label');

    });

}); 