import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react';
import { FeedbackErrorScreen } from '../views/Feedback/FeedbackErrorScreen'

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  useTranslation: () => {
    return {
      t: (str) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('FeedbackErrorScreen', () => {

  test('FeedbackErrorScreen render', () => {

    const onClickClose = jest.fn();

    expect(

      render(

        <FeedbackErrorScreen onClickClose={onClickClose} />

      )

    ).toMatchSnapshot();

    fireEvent(
        screen.getByText('feedbackErrorScreen.buttonText'),
        new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
        })
    );
    
    expect(onClickClose).toBeCalledTimes(1);

    screen.getByText('feedbackErrorScreen.title');    
    
  });

}); 