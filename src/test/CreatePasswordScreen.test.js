import React from 'react'
import { render, screen } from '@testing-library/react';
import { CreatePasswordScreen } from '../views/ProductInformation/CreatePasswordScreen'

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  useTranslation: () => {
    return {
      t: (str) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('CreatePasswordScreen', () => {

  test('CreatePasswordScreen render', () => {

    const onClickClose = jest.fn();

    expect(

      render(

        <CreatePasswordScreen onClickClose={onClickClose} />

      )

    ).toMatchSnapshot();

    screen.getByText('createPasswordScreen.title');    
    
  });

}); 