import React from 'react'
import { render, screen } from '@testing-library/react';

import { Button } from '../components/Button'

describe('Button', () => {

  test('Button renders', () => {

    expect(

      render(

        <Button text='Click me!' />

      )

    ).toMatchSnapshot();

    screen.getByText('Click me!');
    
  });

}); 