import React from 'react'
import { fireEvent, render, screen } from '@testing-library/react';
import { FooterLayout } from '../components/FooterLayout'

jest.mock('react-i18next', () => ({
  // this mock makes sure any components using the translate hook can use it without a warning being shown
  useTranslation: () => {
    return {
      t: (str) => str,
      i18n: {
        changeLanguage: () => new Promise(() => {}),
      },
    };
  },
}));

describe('FooterLayout', () => {

  test('FooterLayout renders', () => {

    const onClickClose = jest.fn();
    const onNext = jest.fn();

    expect(

      render(

        <FooterLayout onClickClose={onClickClose} onNext={onNext}/>

      )

    ).toMatchSnapshot();

    fireEvent(
        screen.getByText('footerLayout.leftButton'),
        new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
        })
    );
    
    expect(onClickClose).toBeCalledTimes(1);

    fireEvent(
        screen.getByText('footerLayout.rightButton'),
        new MouseEvent('click', {
            bubbles: true,
            cancelable: true,
        })
    );
    
    expect(onNext).toBeCalledTimes(1);
    
  });

}); 