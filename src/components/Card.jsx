import React from 'react'
import './components.css'

export const Card = ({
  img,
  align,
  text,
}) => {

  return (

    <div className='card' style={{flexDirection: align}} >

       <div >

        <img src={img} alt='img' className='imageCard'/>
        
        <p>{text}</p>

       </div>

    </div>

  )

}
