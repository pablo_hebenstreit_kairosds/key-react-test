import React from 'react'
import ReactDom from "react-dom";
import { CreatePasswordScreen } from '../views/ProductInformation/CreatePasswordScreen'
import { FormScreen } from '../views/Form/FormScreen'
import { FeedbackSuccessScreen } from '../views/Feedback/FeedbackSuccessScreen';
import { FeedbackErrorScreen } from '../views/Feedback/FeedbackErrorScreen';

export const Modal = ({onClickClose}) => {  
  
  const [activeScreen, setActiveScreen] = React.useState('CreatePasswordScreen')
  
  return (

    ReactDom.createPortal(
      <div className='modal'>

        {activeScreen === 'CreatePasswordScreen' &&
          <CreatePasswordScreen onClickClose={onClickClose} onNext={() => setActiveScreen('FormScreen')}/>
        }
        {activeScreen === 'FormScreen' &&
          <FormScreen onClickClose={onClickClose} onNext={(nextStep) => setActiveScreen(nextStep)} />
        }
        {activeScreen === 'FeedbackSuccessScreen' &&
          <div className='feedback'>
            <FeedbackSuccessScreen onClickClose={onClickClose} onNext={(nextStep) => setActiveScreen(nextStep)}/>
          </div>
        },
        {activeScreen === 'FeedbackErrorScreen' &&
          <div className='feedback'>
          <FeedbackErrorScreen onClickClose={onClickClose} onNext={(nextStep) => setActiveScreen(nextStep)} />
          </div>
        }

      </div>,
      document.querySelector("#modal")
    )
       
  )
  
}
