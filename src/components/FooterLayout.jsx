import React from 'react'
import './components.css'
import { useTranslation } from "react-i18next";
import{ Button } from './Button'

export const FooterLayout = ({onClickClose, onNext}) => {

  const { t } = useTranslation('translation');

  return (

    <div className='footer'>

      <Button 
          backgroundColor='white'
          border='none'
          color='#002B45'
          onClick={onClickClose}
          text={t('footerLayout.leftButton')}
      />

      <Button 
          backgroundColor='#002B45'
          color='white'
          height='40px'
          onClick={onNext}
          text={t('footerLayout.rightButton')}
          width='120px'
      />

    </div>

  )
  
}
