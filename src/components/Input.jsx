import React from 'react'
import './components.css'
import eye from '../assets/img/eye-visible.svg'
import eyeCrossed from '../assets/img/eye-non-visible.svg'

export const Input = ({
  id,
  value,
  onChange,
  validate,
  placeholder,
  label = "",
  border,
  color,
  height,
  radius,
  width,
  padding
}) => {


  const [showImage, setShowImage] = React.useState(false);
  const [inputType, setInputType] = React.useState(false);

  const toggleVisibility = () => {
    setShowImage(showImage ? false : true)
    setInputType(inputType ? false : true)
  }

  return (

    <div className='inputContainer'>
    <img 
      src={showImage ? eye : eyeCrossed} 
      alt='img' 
      className='inputImage' 
      onClick={toggleVisibility}
    />
      <strong>{label}</strong>
      <input 
        type={inputType ? "text" : "password"}
        id={id}
        value={value}
        onChange={onChange}
        validate={validate}
        placeholder={placeholder}
        label={label}
        style={{
          backgroundColor: color,
          border,
          color,
          borderRadius: radius,
          height,
          width,
          padding
        }}
      />
    </div>

  )
  
}
