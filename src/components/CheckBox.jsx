import React from 'react'
import './components.css'

export const CheckBox = ({label, isChecked, handleOnChange, id, name, value}) => {

    

    return (

        <div className="checkboxContainer">

            <div className="">

            <input
                type="checkbox"
                id={id}
                name={name}
                value={value}
                checked={isChecked}
                onChange={handleOnChange}
                />

            </div>

            {label}            

        </div>

    )

}