import React from "react";


export const Button = ({ 
    border,
    backgroundColor,
    color,
    text,
    width,
    height,
    onClick, 
    radius,
    marginTop,
    disabled
  }) => { 
    
  return (

    <button 
      onClick={onClick}
      style={{
         backgroundColor: disabled ? 'gray' : backgroundColor,
         border,
         borderRadius: radius,
         height,
         width,
         marginTop,
         color,
      }}
      disabled={disabled}
    >

      {text}

    </button>

  );

}