import React from 'react'
import './components.css'

export const HeaderLayout = ({title}) => {

  return (

    <div className='headerLayout'>

      <h1>{title}</h1>
      
      <hr  
        width="50px;"
        color= '#79DAE8'
        size="2"
        align="left"
      />

    </div>

  )
  
}
