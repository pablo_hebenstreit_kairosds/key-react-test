import React from 'react'
import { HeaderLayout } from '../components/HeaderLayout'
import { FooterLayout } from '../components/FooterLayout'

export const MainLayout = ({title, children, onClickClose, onNext}) => {

    return (

        <>

            <HeaderLayout title={title}/>

            {children}

            <hr style={{borderTop: '1px solid lightgray'}} />

            <FooterLayout onClickClose={onClickClose} onNext={onNext}/>
            
        </>
        
    )
    
}
