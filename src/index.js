import React from 'react';
import './index.css';
import { createRoot } from 'react-dom/client';
import { I18nextProvider } from 'react-i18next';
import i18next from 'i18next';
import App from './App';
import translation_es from './locale/es/translation.json'
import translation_en from './locale/en/translation.json'

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

i18next.init({
  interpolation: {
    escapeValue: false
  },
  lng: 'es', 
  resources: {
    es: {
      translation: translation_es
    },
    en: {
      translation: translation_en
    },
  }
})

root.render(

  <I18nextProvider i18n={i18next}>

    <App />

  </I18nextProvider>
  
);
