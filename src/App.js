import React from "react";
import "./App.scss";
import { useTranslation } from "react-i18next";
import { WelcomeScreen } from "./views/WelcomeScreen/WelcomeScreen.jsx"

const App =  () => {

    const { i18n } = useTranslation('translation');
    const changeLanguage = (language) => {
        i18n.changeLanguage(language)
    } 

    return(

        <div className="App">

            <main className="App-content">

                <WelcomeScreen changeLanguage={changeLanguage}/>
            
            </main>

        </div>

    );

}

export default App;

